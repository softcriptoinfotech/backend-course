// Loop
var students = ['Rajiv', 'Amit', "Kapil", 'Uttam'];

function sayHello(index, studentName) {
    if (index % 2 != 0) {
        console.log(studentName);
    }
}

function start() {
    for (var i = 0; i < students.length; i++) {
        sayHello(i, students[i]);
    }
}
start();